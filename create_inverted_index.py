import os
import json
import nltk
from collections import defaultdict
from nltk.stem.snowball import EnglishStemmer 


g_counter = 0

class InvertedIndex(object):
    def __init__(self):
        self.index = defaultdict(dict)
        self.documents = defaultdict(dict)
        self.stopwords = nltk.corpus.stopwords.words('english')
        self.stopwords += [",","(",")","[","]","{","}","#","@","!",":",";",".","?"]
        self.stemmer = EnglishStemmer() 
        self.__id__ = 0
    def save_to_json(self):
        with open('inverted_index_beta.json', 'w') as outfile:
            json.dump(self.index, outfile)
        with open('docs_len.json', 'w') as outfile:
            json.dump(self.documents, outfile)
    def add_document(self, document, title, path):
        counter = 0
        global g_counter
        for token in [t.lower() for t in nltk.word_tokenize(document)]:
            if token in self.stopwords:
                continue
            if self.stemmer:
                token = self.stemmer.stem(token)
                counter += 1
 
            # If token is not in the index
            if not token in self.index:
                self.index[token]['frequency'] = 0
            if not self.__id__ in self.index[token]:
                self.index[token][self.__id__] = {}
                self.index[token][self.__id__]['path'] = path
                self.index[token][self.__id__]['title'] = title
                self.index[token][self.__id__]['frequency'] = 0
            self.index[token][self.__id__]['frequency'] += 1
            self.index[token]['frequency'] += 1 
        self.documents[self.__id__]['path'] = path
        self.documents[self.__id__]['length'] = counter
        self.__id__ += 1
        print(g_counter),
        print("added :)")
        g_counter += 1

scripts_directory = './Guiones'
movies_scripts_paths = os.listdir(scripts_directory)
ix = InvertedIndex()

for file_name in movies_scripts_paths:    
    with open(scripts_directory + '/' + file_name, mode='r', encoding='utf-8')\
    as file:
        ix.add_document(file.read(), file_name[7:], \
        scripts_directory + '/' + file_name)
ix.save_to_json()
