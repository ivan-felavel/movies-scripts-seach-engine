import json
import argparse
import nltk
from nltk.stem.snowball import EnglishStemmer

stopwords = nltk.corpus.stopwords.words('english') + [",","(",")","[","]","{","}","#","@","!",":",";",".","?"]
stemmer = EnglishStemmer()

with open("inverted_index_beta.json") as json_file:
    IX = json.load(json_file)

with open("docs_len.json") as json_file:
    DOCS = json.load(json_file)

def get_lexicon_size():
    return len(IX)

def get_doc_path(doc_id):
    return DOCS[doc_id]['path']

def get_doc_len(doc_id):
    return DOCS[doc_id]['length']

def count_word_in_doc(word, document):
    if word in IX: 
        if document in IX[word]:
            return(IX[word][document]['frequency'])
    return 0

def get_word_frequency(word):
    if word in IX:
        return IX[word]['frequency']
    return 0    


def run(file_name, limit):
    proc_query = []
    with open(file_name) as file:
        query = file.read()

    for token in [t.lower() for t in nltk.word_tokenize(query)]:
        if token in stopwords:
            continue
        if stemmer:
            token = stemmer.stem(token)
            proc_query.append(token)

    max_score = 0.0
    alpha = 0.1
    results = []
    for document_key in DOCS:
        score = 1
        for word in proc_query:
            score *= 1000 * (1 - alpha) * (count_word_in_doc(word, document_key) / get_doc_len(document_key))\
                    + alpha * (get_word_frequency(word) / get_lexicon_size())
            # score *= count_word_in_doc(word, document_key) / get_doc_len(document_key)
        if score >= max_score:
            results.append({"score" : score, "path" : get_doc_path(document_key)}) 
            max_score = score

    if len(results) < limit:
        for i in results[::-1]:
            print(i)  
    else:
        for i in range(len(results) - 1, len(results) - limit - 1, -1):
            print(results[i])

def main():
    parser = argparse.ArgumentParser(description='Information retrieval using a simple query likelihood retrieval model.')
    parser.add_argument("file_path", help="screenplay file")
    parser.add_argument("limit", help="results limit")
    args = parser.parse_args()
    file_name = args.file_path
    limit = args.limit
    run(file_name, int(limit))

if __name__ == '__main__':
    main()
