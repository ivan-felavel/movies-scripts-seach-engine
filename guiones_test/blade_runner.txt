    CORRIDOR - TENTH FLOOR - NIGHT 106 Deckard looks around the corner of the door down the hall.
    BATTY'S VOICE Come on, Deckard, show me what you got! I'm right here on the other side of the door.
    SEBASTIAN'S APARTMENT LOBBY - NIGHT 96 Not much to see, But Deckard misses none of it as he crosses the floor and positions himself in the spot of least exposure.
    But you gotta shoot straight 'cause I'm fast! Deckard gets to the door, BLASTS it, kicks it open and FIRES at Batty.
    Deckard looks like he might ask her to stay, but... RACHAEL Good night.
    SEBASTIAN'S APARTMENT - NIGHT 95 In the dim, nocturnal light, Deckard crosses into the courtyard fronting the building and stops.
    BATTY This is how we do it up there, lad! Come on! In a blue of lightning-like action, Batty whips down the hall, zigzagging off the walls towards Deckard so fast that Deckard gets only three SHOTS off before the blur crashes through the wall on his left with a laugh.
    CORRIDOR DECKARD'S APARTMENT - NIGHT 52 He's coming down the hall carrying a foil wrapped plastic plate and stops in front of his door.
    BATTY'S VOICE Two! Deckard darts into the nearest door.
    Maybe if he were on firmer ground he might do something about such an offer but... Deckard's eyes follow her down as Rachael bends to the floor and starts picking the food off the rug, put- ting it back on the plate.
    PASSAGEWAY - NIGHT 65 Deckard bounds out of the room and sees her go through a door at the other end of the hall.
    TENTH FLOOR APARTMENT - NIGHT 111 Deckard's looking for a corner -- a place that covers the angles.
    Tyrell stands silhouetted behind Deckard, who sits in front of Rachael, a pencil beam trained on her eye.
    DECKARD'S APARTMENT BEDROOM - DAY 81 Rachael is lying across the bed in one of Deckard's shirts, her chin over the edge, her eyes moving around the room.
    LEON'S ROOM - NIGHT 18 Deckard pockets the pictures and moves away from the window.
    DECKARD It wasn't very good last night, was it? RACHAEL I don't know, I have nothing to compare it to.
    TENTH FLOOR APARTMENT - NIGHT 115 Deckard's crouched lower, holding his breath -- talk about a hair trigger... Silence.
    Batty's next to him, grabs Deckard's hand and steps in closer.
    CORRIDOR - NIGHT 100 Laser in hand, Deckard kicks open the swinging doors and walks into the corridor, a dangerous man.
    Deckard tips his head good night and backs out of the door.